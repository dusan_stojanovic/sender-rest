package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type MessageDto struct {
	Content string `json:"content,omitempty"`
}

func main() {
	message := MessageDto{
		Content: "Hello World!!!",
	}
	body, _ := json.Marshal(message)

	for i := 0; i < 10; i++ {
		resp, err := http.Post(
			"http://localhost:8080/api/messages", "application/json", bytes.NewBuffer(body))
		if err != nil {
			fmt.Println("Error: ", err)
		}
		fmt.Println("Response status: ", resp.Status)
		time.Sleep(5 * time.Second)
		resp.Body.Close()
	}
}
